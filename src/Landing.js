import React, {Component} from 'react';
import './Landing.css';
import App from './App.js';
import ReactDOM from 'react-dom';
import Card from "./Card";


class Landing extends Component {

    constructor (props) {
        super(props);
        this.state = {
            selected : "",
            userName : "John",
            points : 400,
            data : require('./questionListing.json'),
            completed : {

            }
        };
        this.data = require('./questionListing.json');

    }

    gridStyle = {
        position: 'absolute',
        left: '0px',
        top: '0px'
    };

    Quiz = () => {
        ReactDOM.render(
            <App/>,
            document.getElementById('root')
        );
    };




    render() {
        this.cardList = [];
        for (this.i = 0; this.i < this.props.level; this.i++) {
            this.cardList.push(
                <Card
                    name= {this.data[this.i].name}
                    level= {this.data[this.i].level}
                    color= {this.data[this.i].color}
                    background= {this.data[this.i].background}
                />
            );
        }
        return [

            <div id="SideBar" className="col-md-3">
                <div id="SideBarContent">
                    <div>
                        <p>

                        </p>
                    </div>
                    <h3>
                        Welcome Back <a>{this.state.userName}</a>!
                    </h3>
                    <h5>Level: 2</h5>
                    <p>You have mastered <b>two</b> levels.  Select another module and master more skills!</p>
                    <label htmlFor="SelectForm">Make your selection</label>
                    <select className="form-control" id="SelectForm">
                        {this.state.data.map((item, index) => (
                            <option> {item.name} </option>
                        ))}
                    </select>
                    <button  type="button" className="btn btn-primary" onClick={this.Quiz}>Begin</button>
                </div>

            </div>,
            <div id="TrophySection" className="col-md-9">
                <div id="TrophyContent" className="container-fluid">
                    {
                        this.state.data.map((item, index) => (
                            <Card
                                name= {item.name}
                                level= {item.level}
                                color= {item.color}
                                background= {item.background}
                            />
                        ))
                    }
                </div>
            </div>
        ]
    }
}

export default Landing;
