import React, {Component} from 'react';
import './App.css';

class Button extends Component {
    outline = "";
    handleClick = () => this.props.onClick(this.props.index);

    clearStyle = () => {
        this.outline = "";
    };

    outlineButton = (style) => {
        this.outline = style;
    };

    render() {

        if (this.outline !== "") {
            this.className = this.outline;
        }
        else {
            if (!this.props.active) {
                this.props.isActive ? this.className = 'btn center btn-outline-success btn-sm active' : this.className = 'btn center btn-outline-secondary btn-sm';
            }
        }

        return <button
            type='button'
            className={
                this.className
            }
            onClick={this.handleClick}
        >
            <span>{this.props.name}</span>
        </button>
    }
}

export default Button;
