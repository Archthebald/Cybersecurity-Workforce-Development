import React, {Component} from 'react'

class Card extends Component {
    constructor (props) {
        super(props);

        this.state = {
            name : "",
            level : 0,
            color: "",
            picture: "",
            background: ""
        };

        //change to setstate
        this.state.name = props.name;
        this.state.level = props.level;
        this.state.color = props.color;
        this.state.background = props.background;

    }

    oneStar = "./icons/i.png";
    twoStar = "./icons/ii.png";
    threeStar = "./icons/iii.png";

    render() {
        if (this.state.level == 1) {
            this.state.picture = this.oneStar;
        } else if (this.state.level == 2){
            this.state.picture = this.twoStar;

        }else if (this.state.level == 3) {
            this.state.picture = this.threeStar;
        }
        return (
            <div className="card">
                <img className="star" src={this.state.picture}></img>
                {/*<div className="star">  </div>*/}
                <div className={"card-"+this.state.color}>
                    <div className="circle" style={ { backgroundImage: "url("+this.state.background+")" } }></div>
                </div>
                <div className="card-description">{this.state.name}</div>
            </div>
        )

    }

}
export default Card;