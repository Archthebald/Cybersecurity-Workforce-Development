import 'rc-slider/assets/index.css';

import React, {Component} from 'react'
import Slider from 'rc-slider';

class Horizontal extends Component {

    constructor(props) {
        super(props);
        this.state = {
            min: 0,
            max: 20,
            value: 0,
            color: "black",
        };
    }


    onSliderChange = (value) => {

        this.setState({
            value,
        });

        if (value < 5) this.setState({color: "green"});

        else if (value < 10) this.setState({color: "gold"});

        else if (value < 15) this.setState({color: "orange"});

        else this.setState({color: "red"});

    };


    setValue = (a) => {
        this.setState({
            value: a,
            color: "black"
        })
    };


    render() {
        return (
            <div className={this.props.visible ? "visible" : "invisible"} >
            <Slider value={this.state.value}
                    onChange={this.onSliderChange}
                    min={this.state.min}
                    disabled={this.props.active}
                    max={this.state.max}
                    onAfterChange={this.props.onAfterChange}
                    handleStyle={{
                        borderColor: this.state.color,
                        height: 28,
                        width: 28,
                        marginLeft: -14,
                        marginTop: -9,
                        backgroundColor: 'white'
                    }}
                    railStyle={{backgroundColor: 'black', height: 10, visibility:'inherit'}}
                    trackStyle={{backgroundColor: this.state.color, height: 10, visibility:'inherit'}}
            />
            </div>
        );
    }

    s
}

export default Horizontal;