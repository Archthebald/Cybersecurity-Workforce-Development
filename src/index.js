import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Landing from './Landing';

// ReactDOM.render(
//     <App/>,
//     document.getElementById('root')
// );

ReactDOM.render(
    <Landing/>,
    document.getElementById('root')
);