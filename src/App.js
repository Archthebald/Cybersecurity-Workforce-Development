import React, {Component} from 'react';
import Horizontal from './Slider';
import Button from './Button';
import docx from '../public/icons/docx.png';
import './App.css';

class App extends Component {

    handleClick = (index) => {
        this.setState({selectedAction: index});
    };

    handleChange = (value) => {
        this.setState({threatLevel: value});
        // console.log(value);
    };

    HandleResponse = () => {
        if (!this.state.responseSubmitted) {
            if (this.state.selectedAction !== null) {

                if (this.state.selectedAction === this.data[this.state.questionIndex].correctresponse) {
                    this.submitButton.className = "btn btn-lg btn-success"

                }
                else {
                    this.submitButton.className = "btn btn-lg btn-danger";

                    switch (this.data[1].correctresponse) {
                        case 1:
                            this.index1.outlineButton("btn center btn-outline-success btn-sm");
                            break;
                        case 2:
                            this.index2.outlineButton("btn center btn-outline-success btn-sm");
                            break;
                        case 3:
                            this.index3.outlineButton("btn center btn-outline-success btn-sm");
                            break;
                        case 4:
                            this.index4.outlineButton("btn center btn-outline-success btn-sm");
                            break;
                        default:
                            break;
                    }

                    switch (this.state.selectedAction) {
                        case 1:
                            this.index1.outlineButton("btn center btn-outline-danger btn-sm");
                            break;
                        case 2:
                            this.index2.outlineButton("btn center btn-outline-danger btn-sm");
                            break;
                        case 3:
                            this.index3.outlineButton("btn center btn-outline-danger btn-sm");
                            break;
                        case 4:
                            this.index4.outlineButton("btn center btn-outline-danger btn-sm");
                            break;
                        default:
                            break;
                    }
                }
                this.sugestedslider.setValue(this.data[this.state.questionIndex].scale);

                this.setState({responseSubmitted: true});
                this.setState({styleCondition: true});
            }

            if (Math.abs(this.state.threatLevel - this.data[this.state.questionIndex].scale) > 1) {

            }
            else if (Math.abs(this.state.threatLevel - this.data[this.state.questionIndex].scale) > 1) {

            }
        }
    };

    Feedback = () => {

    };

    Advance = () => {
        if (this.state.questionIndex+1 === this.data.length){
            this.setState({styleCondition: false});
            alert("Thank you for playin");
        }
        else {
            this.index1.clearStyle();
            this.index2.clearStyle();
            this.index3.clearStyle();
            this.index4.clearStyle();
            this.setState({
                responseSubmitted: false,
                selectedAction: null,
                threatLevel: null,
                isVisible: false,
                styleCondition: false
            });
            this.submitButton.className = "btn btn-secondary btn-lg ";
            this.setState({questionIndex: this.state.questionIndex + 1});
            this.slider.setValue(0);
        }

    };

    constructor(props) {
        super(props);
        this.state = {
            responseSubmitted: false,
            questionIndex: 1,
            selectedAction: null,
            threatLevel: null,
            isVisible: false,
            styleCondition: false
            // styleCondition: true
        };
        this.data = require('./Data.json');
    }

    render() {
        return [
            <div id="ResponseSection" className="col-md-3">

                <div id="Questions" className=" text-center">
                    <div id="Information">
                        <p id="questionDescription">
                            {/*TODO: fill in instructions and reformat section*/}
                            Evaluate the email to the right and determine how much of a security risk this poses, and
                            what action is to be taken.
                        </p>
                    </div>
                    <div>
                        <div id="Slider">
                            <h2>Threat Level</h2>
                            <Horizontal
                                visible={true}
                                active={this.state.responseSubmitted}
                                ref={foo => this.slider = foo}
                                onAfterChange={this.handleChange}
                            />
                            <br/>
                            {console.log(this.data[this.state.questionIndex].scale)}
                            <Horizontal
                                visible={this.state.styleCondition}
                                active={true}
                                ref={foo => this.sugestedslider = foo}
                            />
                        </div>
                        <div id="Responses">
                            <h2>Action to be taken </h2>
                            <div className="row">
                                <Button ref={foo => this.index1 = foo}
                                        active={this.state.responseSubmitted}
                                        name={this.data[this.state.questionIndex].responses[0]} index={1}
                                        isActive={this.state.selectedAction === 1} onClick={this.handleClick}/>
                                <Button ref={foo => this.index2 = foo}
                                        active={this.state.responseSubmitted}
                                        name={this.data[this.state.questionIndex].responses[1]} index={2}
                                        isActive={this.state.selectedAction === 2} onClick={this.handleClick}/>

                            </div>
                            <div className="row">
                                <Button ref={foo => this.index3 = foo}
                                        active={this.state.responseSubmitted}
                                        name={this.data[this.state.questionIndex].responses[2]} index={3}
                                        isActive={this.state.selectedAction === 3} onClick={this.handleClick}/>
                                <Button ref={foo => this.index4 = foo}
                                        active={this.state.responseSubmitted}
                                        name={this.data[this.state.questionIndex].responses[3]} index={4}
                                        isActive={this.state.selectedAction === 4} onClick={this.handleClick}/>

                            </div>
                            <div id="Submit">
                                <button ref={(a) => this.submitButton = a} onClick={() => this.HandleResponse()}
                                        className="btn btn-secondary btn-lg ">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>,
            <div id="EmailClient" className="col-md-9">
                <div id="EmailContent">
                {/*TODO: style like roundcube,  add dividers*/}
                    <div id="prompt" className="col">
                        <hr/>
                        <p id="date" className="text-right">Date: {this.data[this.state.questionIndex].date}</p>
                        <hr/>
                        <p id="from" onClick={() => this.index2.handleClick(1)} className="text-right">
                            from: {this.data[this.state.questionIndex].sender}</p>
                        <hr/>
                        <p id="to" className="text-right">to: {this.data[this.state.questionIndex].recepient}</p>
                        <hr/>
                        <p id="subject" className="text-right">subject: {this.data[this.state.questionIndex].title}</p>
                        <hr/>
                        <p id="EmailBody" className="">{this.data[this.state.questionIndex].message}</p>
                        <hr/>
                        <div id="attachmentImage" onClick={() => this.index1.handleClick(1)}>
                            <img className="image-attachment" alt="" src={docx}/>
                            <a>{this.data[this.state.questionIndex].atachements}</a>
                        </div>

                    </div>
                </div>
                <div id="responsBar" className={this.state.styleCondition ? "show" : "hide"}>
                    <div className="container-fluid" id="responsBarContent">
                        <div className="row">
                            <div className="col-lg-10">
                                Response section needs data and formatting.
                            </div>
                            <div className="col-lg-2" id="text-center">
                                <button id="proceedButton" className="btn btn-secondary btn-lg " onClick={this.Advance}>Continue</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        ];
    }
}


export default App;
